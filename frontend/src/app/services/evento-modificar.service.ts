import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};

@Injectable({
  providedIn: 'root'
})
export class EventoModificarService {

  //1-paso importar el httpClient y agregarlos en los import de app.module.ts
  constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

   //tipo de servicio GET
   modificarEvento(titulo,descripcion,imagen,precio,fecha,duracion,capacidad,modalidad,id_eventos): Observable<any>{
    const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
     titulo : titulo,
     descripcion : descripcion,
     imagen : imagen,
     precio : precio,
     fecha : fecha,
     duracion : duracion,
     capacidad : capacidad,
     modalidad : modalidad,
     id_eventos : id_eventos, 
    };
    const newSession = Object.assign({},parametros);//es la variable de sesion
   //console.log(parametro)
   return this.http.put<any>(`http://localhost:3500/eventos/evento/modificarCurso/${id_eventos}`,newSession,cudOptions)
  }
}
