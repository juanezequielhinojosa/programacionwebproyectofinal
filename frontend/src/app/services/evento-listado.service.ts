import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};

@Injectable({
  providedIn: 'root'
})
export class EventoListadoService {
  //1-paso importar el httpClient y agregarlos en los import de app.module.ts
  constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

   //tipo de servicio GET
   getEventosDeUsuario (parametro): Observable<any>{
   //console.log(parametro)
    return this.http.get(`http://localhost:3500/eventos/listarPorIdUsuarioLogueado/${parametro}`)
  }

   //tipo de servicio GET del node
   getEventos (): Observable<any>{
    
  
    return this.http.get<any>('http://localhost:3500/eventos/',cudOptions)
  }
}

