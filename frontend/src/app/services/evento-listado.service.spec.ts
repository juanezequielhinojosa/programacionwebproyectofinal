import { TestBed } from '@angular/core/testing';

import { EventoListadoService } from './evento-listado.service';

describe('EventoListadoService', () => {
  let service: EventoListadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoListadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
