import { TestBed } from '@angular/core/testing';

import { EventoBorradoService } from './evento-borrado.service';

describe('EventoBorradoService', () => {
  let service: EventoBorradoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoBorradoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
