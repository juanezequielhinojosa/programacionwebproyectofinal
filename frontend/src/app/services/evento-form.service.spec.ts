import { TestBed } from '@angular/core/testing';

import { EventoFormService } from './evento-form.service';

describe('EventoFormService', () => {
  let service: EventoFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
