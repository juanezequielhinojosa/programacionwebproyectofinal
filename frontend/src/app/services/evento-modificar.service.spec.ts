import { TestBed } from '@angular/core/testing';

import { EventoModificarService } from './evento-modificar.service';

describe('EventoModificarService', () => {
  let service: EventoModificarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoModificarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
