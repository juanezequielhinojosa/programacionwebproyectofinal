import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};
@Injectable({
  providedIn: 'root'
})
export class EventoBorradoService {

   //1-paso importar el httpClient y agregarlos en los import de app.module.ts
   constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

   //tipo de servicio POST
   eliminarEventoDeUsuario(id): Observable<any>{
    
     return this.http.delete(`http://localhost:3500/eventos/evento/eliminar/${id}`)
   }
 
}

