import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json; charset=utf-8'}),//application/json
  
};

@Injectable({
  providedIn: 'root'
})
export class EventoBuscadoService {

  //1-paso importar el httpClient y agregarlos en los import de app.module.ts
  constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

  //tipo de servicio geT
  getBuscar (id): Observable<any>{
   
    const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
      id : id,
      
    };
    //const newSession = Object.assign({},parametros);//es la variable de sesion
    return this.http.get<any>('http://localhost:3500/eventos/evento',parametros.id)
  }

}
