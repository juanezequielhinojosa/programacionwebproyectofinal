import { TestBed } from '@angular/core/testing';

import { InscriptosFormService } from './inscriptos-form.service';

describe('InscriptosFormService', () => {
  let service: InscriptosFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InscriptosFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
