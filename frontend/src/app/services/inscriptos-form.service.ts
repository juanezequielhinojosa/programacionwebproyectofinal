import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios


///encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};
@Injectable({
  providedIn: 'root'
})
export class InscriptosFormService {

  //1-paso importar el httpClient y agregarlos en los import de app.module.ts
  constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

  //tipo de servicio POST
  postInscripcion(id_inscripciones,nombre,apellido,dni,celular,codigo_pago,codigo_inscripcion,eventos_id): Observable<any>{
    const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
      id_inscripciones : id_inscripciones,
      nombre : nombre,
      apellido : apellido,
      dni : dni,
      celular : celular,
      codigo_pago : codigo_pago,
      codigo_inscripcion : codigo_inscripcion,
      eventos_id : eventos_id
      

    };
    const newSession = Object.assign({},parametros);//es la variable de sesion
    return this.http.post<any>('http://localhost:3500/eventos/evento/inscripcion',newSession,cudOptions)
  }

}
