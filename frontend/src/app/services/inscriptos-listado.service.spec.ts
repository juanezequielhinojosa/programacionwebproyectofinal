import { TestBed } from '@angular/core/testing';

import { InscriptosListadoService } from './inscriptos-listado.service';

describe('InscriptosListadoService', () => {
  let service: InscriptosListadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InscriptosListadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
