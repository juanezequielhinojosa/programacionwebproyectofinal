import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios


//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};

@Injectable({
  providedIn: 'root'
})
export class EventoFormService {
  //1-paso importar el httpClient y agregarlos en los import de app.module.ts
  constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

   //tipo de servicio POST
   enviarEvento (id_eventos,titulo,descripcion,imagen,precio,fecha,duracion,capacidad,modalidad,ponente1,ponente2,ponente3,link1,link2,link3,id_usuarios): Observable<any>{
    const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
      id_eventos : id_eventos,
      titulo : titulo,
      descripcion : descripcion,
      imagen : imagen,
      precio : precio,
      fecha  : fecha,
      duracion : duracion,
      capacidad : capacidad,
      modalidad : modalidad,
      ponente1  : ponente1,
      ponente2 : ponente2,
      ponente3  : ponente3,
      link1 : link1,
      link2 : link2,
      link3 : link3,
      id_usuarios : id_usuarios,

    };
    const newSession = Object.assign({},parametros);//es la variable de sesion
    return this.http.post<any>('http://localhost:3500/eventos/evento',newSession,cudOptions)
  }
}
