import { TestBed } from '@angular/core/testing';

import { EventoBuscadoService } from './evento-buscado.service';

describe('EventoBuscadoService', () => {
  let service: EventoBuscadoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EventoBuscadoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
