import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';//permite manejar los observables q son las respuestas de los servicios

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};

@Injectable({
  providedIn: 'root'
})
export class UsuarioFormService {

   //1-paso importar el httpClient y agregarlos en los import de app.module.ts
   constructor(public http:HttpClient) { }//creamos un objeto de tipo httpClient

   //tipo de servicio POST
   postUsuario (email,password): Observable<any>{
     const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
       email : email,
       password : password
     };
     const newSession = Object.assign({},parametros);//es la variable de sesion
     return this.http.post<any>('http://localhost:3500/eventos/evento/registrar',newSession,cudOptions)
   }
}

