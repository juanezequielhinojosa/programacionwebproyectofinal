import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

//encabezado
const cudOptions={
  headers: new HttpHeaders({'Content-Type':'application/json'}),
};

@Injectable({
  providedIn: 'root'
})
export class InscriptosListadoService {

  constructor(public http:HttpClient) { }

  //tipo de servicio POST
  postInscriptos (id): Observable<any>{
    const parametros = {//Es la misma estructura q usamos en postman, y es lo q voy a enviar
      id : id,
    };
    const newSession = Object.assign({},parametros);//es la variable de sesion
    return this.http.post<any>('http://localhost:3500/eventos/inscriptos',newSession,cudOptions)
  }
}





