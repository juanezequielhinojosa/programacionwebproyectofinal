import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoDetalleAsistenteComponent } from './evento-detalle-asistente.component';

describe('EventoDetalleAsistenteComponent', () => {
  let component: EventoDetalleAsistenteComponent;
  let fixture: ComponentFixture<EventoDetalleAsistenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoDetalleAsistenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoDetalleAsistenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
