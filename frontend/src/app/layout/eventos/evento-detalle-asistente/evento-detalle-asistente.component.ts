import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';//Router,private router:Router,


@Component({
  selector: 'app-evento-detalle-asistente',
  templateUrl: './evento-detalle-asistente.component.html',
  styleUrls: ['./evento-detalle-asistente.component.css']
})
export class EventoDetalleAsistenteComponent implements OnInit {

  parametros={};
  titulo='';                                 
  descripcion='';
  imagen='';
  precio='';
  id_eventos='';
  link1='';
  fecha='';
  duracion='';
  capacidad='';
  modalidad='';

  constructor(private route:ActivatedRoute,private router:Router) {

   }

   ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      this.parametros=params;
      console.log(this.parametros);
      this.titulo=params['titulo']
      this.descripcion=params['descripcion']
      this.imagen=params['imagen']
      this.precio=params['precio']
      this.link1=params['link1']
      this.fecha=params['fecha']
      this.duracion=params['duracion']
      this.capacidad=params['capacidad']
      this.modalidad=params['modalidad']
      this.id_eventos=params['id_eventos']
      //alert(this.titulo)
    });
  }
  submitInscripcionForm(){
    console.log(this.id_eventos)
    this.router.navigate(['/inscripcion'],{ 
      queryParams:{
        'eventoId':this.id_eventos
      }
    });
  }



}
