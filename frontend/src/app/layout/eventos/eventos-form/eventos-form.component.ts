import { Component, OnInit } from '@angular/core';
import { EventoFormService } from 'src/app/services/evento-form.service';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-eventos-form',
  templateUrl: './eventos-form.component.html',
  styleUrls: ['./eventos-form.component.css']
})
export class EventosFormComponent implements OnInit {

   //importamos el servicio  usuariosService
  //creamos un objeto router de la clase Router q nos permitira redireccionar a una determinada pagina 
  constructor(private route:ActivatedRoute,private router:Router,private eventoformService:EventoFormService) { }
  
  id_eventos ='';
  titulo =''; 
  descripcion ='';
  imagen ='';
  precio ='';
  fecha  ='';
  duracion ='';
  capacidad ='';
  modalidad ='';
  ponente1 ='';
  ponente2 ='';
  ponente3  ='';
  link1 ='';
  link2 ='';
  link3 ='';
  id_usuarios ='';
  
///estas son las variables q bindeamos con el html

  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      console.log(params)
      this.id_usuarios=params['id']
 
    })
  }
  submit2(){
   this.eventoformService.enviarEvento(this.id_eventos,this.titulo,this.descripcion,this.imagen,this.precio,this.fecha,this.duracion,this.capacidad,this.duracion,this.ponente1,this.ponente2,this.ponente3,this.link1,this.link2,this.link3,this.id_usuarios).subscribe(respuesta => {
      console.log(respuesta)
      if(respuesta['ok']==true){
         alert('se agrego correctamente')
         this.router.navigate(['/']);
     }else{
      alert('error intente mas tarde');
     }
     
  
   });
  }


}
