import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoListadoCompletoComponent } from './evento-listado-completo.component';

describe('EventoListadoCompletoComponent', () => {
  let component: EventoListadoCompletoComponent;
  let fixture: ComponentFixture<EventoListadoCompletoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoListadoCompletoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoListadoCompletoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
