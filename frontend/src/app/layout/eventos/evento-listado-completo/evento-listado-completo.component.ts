import { Component, OnInit } from '@angular/core';
import { EventoListadoService } from 'src/app/services/evento-listado.service';
import { Router} from '@angular/router';
@Component({
  selector: 'app-evento-listado-completo',
  templateUrl: './evento-listado-completo.component.html',
  styleUrls: ['./evento-listado-completo.component.css']
})
export class EventoListadoCompletoComponent implements OnInit {

  constructor(private router:Router,private eventoListadoService:EventoListadoService) { }//private eventoBuscadoService:EventoBuscadoService
  eventos: any []=[];

  ngOnInit(): void {

    this.eventoListadoService.getEventos().subscribe(respuesta => {
      console.log(respuesta)
      this.eventos=respuesta
    })
  }

  submitIndice(i): void {
    //alert(i)
    
    this.router.navigate(['/detalleEventoAsistente'],{ 
      queryParams:{
        'titulo':(this.eventos[i].titulo),
        'descripcion':(this.eventos[i].descripcion),
        'imagen':(this.eventos[i].imagen),
        'precio':(this.eventos[i].precio),
        'link1':(this.eventos[i].link1),
        'fecha':(this.eventos[i].fecha),
        'duracion':(this.eventos[i].duracion),
        'capacidad':(this.eventos[i].capacidad),
        'modalidad':(this.eventos[i].modalidad),
        'id_eventos':(this.eventos[i].id_eventos),
      }
    });
  }

}
