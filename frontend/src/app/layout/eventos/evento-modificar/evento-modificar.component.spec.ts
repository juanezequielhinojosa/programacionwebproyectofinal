import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventoModificarComponent } from './evento-modificar.component';

describe('EventoModificarComponent', () => {
  let component: EventoModificarComponent;
  let fixture: ComponentFixture<EventoModificarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventoModificarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventoModificarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
