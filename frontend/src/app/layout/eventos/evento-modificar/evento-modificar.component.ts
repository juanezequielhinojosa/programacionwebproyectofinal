import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import { EventoModificarService } from 'src/app/services/evento-modificar.service';

@Component({
  selector: 'app-evento-modificar',
  templateUrl: './evento-modificar.component.html',
  styleUrls: ['./evento-modificar.component.css']
})
export class EventoModificarComponent implements OnInit {
  parametros={};
  titulo='';                                 
  descripcion='';
  imagen='';
  precio='';
  fecha='';
  duracion='';
  capacidad='';
  modalidad='';
  id_eventos='';

  constructor(private route:ActivatedRoute,private router:Router,private eventoModificarService:EventoModificarService) {
   
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      this.parametros=params;
      console.log(this.parametros);
      this.titulo=params['titulo']
      this.descripcion=params['descripcion']
      this.imagen=params['imagen']
      this.precio=params['precio']
      this.fecha=params['fecha']
      this.duracion=params['duracion']
      this.capacidad=params['capacidad']
      this.modalidad=params['modalidad']
      this.id_eventos=params['id_eventos']
      //alert(this.id_eventos)
    });
  }

  modificar(){
    this.eventoModificarService.modificarEvento(this.titulo,this.descripcion,this.imagen,this.precio,this.fecha,this.duracion,this.capacidad,this.modalidad,this.id_eventos).subscribe(respuesta => {
      
      console.log(respuesta)
      if(respuesta['ok']==true){
        alert('Se modifico correctamente')
       
        this.router.navigate(['/'],{ 
        });
      }else{
        alert('intente mas tarde');
      }
      
      
    });
  }
}
