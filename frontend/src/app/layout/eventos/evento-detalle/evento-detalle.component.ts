import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';//Router,private router:Router,
import { EventoListadoService } from 'src/app/services/evento-listado.service';
import { EventoBorradoService } from 'src/app/services/evento-borrado.service';

@Component({
  selector: 'app-evento-detalle',
  templateUrl: './evento-detalle.component.html',
  styleUrls: ['./evento-detalle.component.css']
})
export class EventoDetalleComponent implements OnInit {
  parametros={};
  titulo='';                                 
  descripcion='';
  imagen='';
  precio='';
  fecha='';
  duracion='';
  capacidad='';
  modalidad='';
  id_eventos='';

  constructor(private route:ActivatedRoute,private router:Router,private eventoListadoService:EventoListadoService,private eventoBorradoService:EventoBorradoService) {
   
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      this.parametros=params;
      console.log(this.parametros);
      this.titulo=params['titulo']
      this.descripcion=params['descripcion']
      this.imagen=params['imagen']
      this.precio=params['precio']
      this.fecha=params['fecha']
      this.duracion=params['duracion']
      this.capacidad=params['capacidad']
      this.modalidad=params['modalidad']
      this.id_eventos=params['id_eventos']
      //alert(this.id_eventos)
    });
  }

  eliminarEvento():void{

    this.eventoBorradoService.eliminarEventoDeUsuario(this.id_eventos).subscribe(respuesta => {
      alert(this.id_eventos)
      if(respuesta['ok']==true){
        alert('Se Borro Correctamente!!!')
       
        this.router.navigate(['/'],{ 
         
        });
      }else{
        alert('Ahora no es posible realizar la operacion , intente mas tarde');
      }
    });

  }

  submitModificar(): void {

    this.router.navigate(['/evento_modificar'],{ 
      queryParams:{
        'titulo':this.titulo,
        'descripcion':this.descripcion,
        'imagen':this.imagen,
        'precio':this.precio,
        'fecha':this.fecha,
        'duracion':this.duracion,
        'capacidad':this.capacidad,
        'modalidad':this.modalidad,
        'id_eventos':this.id_eventos,
      }
    });
  }

}

