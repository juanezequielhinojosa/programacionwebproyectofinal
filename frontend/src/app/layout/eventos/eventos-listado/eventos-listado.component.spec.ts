import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventosListadoComponent } from './eventos-listado.component';

describe('EventosListadoComponent', () => {
  let component: EventosListadoComponent;
  let fixture: ComponentFixture<EventosListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventosListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventosListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
