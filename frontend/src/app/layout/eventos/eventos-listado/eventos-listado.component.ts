import { Component, OnInit } from '@angular/core';
import { EventoListadoService } from 'src/app/services/evento-listado.service';
import { Router, ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-eventos-listado',
  templateUrl: './eventos-listado.component.html',
  styleUrls: ['./eventos-listado.component.css']
})
export class EventosListadoComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router,private eventoListadoService:EventoListadoService) { }
  eventos: any []=[];
  usuario='';
  parametro:number;
  idDeCuros='';
  
 
  ngOnInit(): void {

    this.route.queryParams.subscribe(params=>{
      this.parametro=params['id']
      this.usuario=params['usuario']
      console.log(this.parametro)
    })
    
      this.eventoListadoService.getEventosDeUsuario(this.parametro).subscribe(respuesta => {
        
        console.log(respuesta)
        this.eventos=respuesta
      });
  }
 
submitIndice(i): void {

  this.router.navigate(['/detalleEvento'],{ 
    queryParams:{
      'titulo':(this.eventos[i].titulo),
      'descripcion':(this.eventos[i].descripcion),
      'imagen':(this.eventos[i].imagen),
      'precio':(this.eventos[i].precio),
      'fecha':(this.eventos[i].fecha),
      'duracion':(this.eventos[i].duracion),
      'capacidad':(this.eventos[i].capacidad),
      'modalidad':(this.eventos[i].modalidad),
      'id_eventos':(this.eventos[i].id_eventos),
    }
  });
}

submitIndice2(i): void {
  
  this.router.navigate(['/listaInscriptos'],{ 
    queryParams:{
      
      'idCurso':(this.eventos[i].id_eventos),
      'titulo':(this.eventos[i].titulo),
    }
  });
}

submitEventoNuevo(): void {
  
  this.router.navigate(['/evento_nuevo'],{ 
  queryParams:{
    'id':this.parametro,
  }
});
  
}
}





