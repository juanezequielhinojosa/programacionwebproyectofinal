import { Component, OnInit } from '@angular/core';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  //importamos el servicio  usuariosService
  //creamos un objeto router de la clase Router q nos permitira redireccionar a una determinada pagina 
  constructor(private router:Router,private usuariosService:UsuariosService) { }
  email='';                                 //este es un objeto que accede al servicio de la clase usuario service
  password='';
  //estas son las variables q bindeamos con el html
  

  ngOnInit(): void {
    //aqui podriamos hacer una prueba
    //es como el evento load
  }
  //////////////////////
  submit(){
    this.usuariosService.postLogin(this.email,this.password).subscribe(respuesta => {
      console.log(respuesta)
      if(respuesta['ok']==true){
        alert(respuesta['mensaje'])
       
        this.router.navigate(['/evento_listado'],{ 
          queryParams:{
            'id':(respuesta['resp'].id_usuarios),
            'usuario':(respuesta['resp'].email)
          }
        });
      }else{
        alert(respuesta['mensaje']);
      }
      
      
    });
  }

}
