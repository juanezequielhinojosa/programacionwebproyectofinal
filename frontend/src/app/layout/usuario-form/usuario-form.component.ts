import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioFormService } from 'src/app/services/usuario-form.service';

@Component({
  selector: 'app-usuario-form',
  templateUrl: './usuario-form.component.html',
  styleUrls: ['./usuario-form.component.css']
})
export class UsuarioFormComponent implements OnInit {

  constructor(private router:Router,private usuarioFormService:UsuarioFormService) { }
  email='';                                 //este es un objeto que accede al servicio
  password='';
  

  ngOnInit(): void {
    //aqui podriamos hacer una prueba
  }
  //////////////////////
  submitUsuario(){
    this.usuarioFormService.postUsuario(this.email,this.password).subscribe(respuesta => {
      console.log(respuesta)
      if(respuesta['ok']==true){
        alert('Bienvenido ,ya podes loguearte y vender tus cursos!!!')
       
        this.router.navigate(['/'],{ 
         
        });
      }else{
        alert('Ahora no es posible registrarse , intente mas tarde');
      }
      
      
    });
  }


}

