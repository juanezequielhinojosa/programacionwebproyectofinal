import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { EventosListadoComponent } from './eventos/eventos-listado/eventos-listado.component';
import { EventosFormComponent } from './eventos/eventos-form/eventos-form.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { InscriptosListadoComponent } from './inscriptos/inscriptos-listado/inscriptos-listado.component';
import { EventoDetalleComponent } from './eventos/evento-detalle/evento-detalle.component';
import { EventoListadoCompletoComponent } from './eventos/evento-listado-completo/evento-listado-completo.component';
import { InscriptosFormComponent } from './inscriptos/inscriptos-form/inscriptos-form.component';
import { EventoDetalleAsistenteComponent } from './eventos/evento-detalle-asistente/evento-detalle-asistente.component';
import { FooterComponent } from './footer/footer.component';
import { UsuarioFormComponent } from './usuario-form/usuario-form.component';
import { EventoModificarComponent } from './eventos/evento-modificar/evento-modificar.component'



@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    EventosFormComponent,
    EventosListadoComponent,
    LoginComponent,
    InscriptosListadoComponent,
    EventoDetalleComponent,
    EventoListadoCompletoComponent,
    InscriptosFormComponent,
    EventoDetalleAsistenteComponent,
    FooterComponent,
    UsuarioFormComponent,
    EventoModificarComponent
    
    
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    CommonModule,
    //RouterModule.forChild(routes),
    FormsModule
  ]
})
export class LayoutModule { }
