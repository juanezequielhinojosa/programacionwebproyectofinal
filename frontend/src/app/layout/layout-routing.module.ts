import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { EventosListadoComponent } from './eventos/eventos-listado/eventos-listado.component';
import { EventosFormComponent } from './eventos/eventos-form/eventos-form.component';
import { LoginComponent } from './login/login.component';
import { InscriptosListadoComponent } from './inscriptos/inscriptos-listado/inscriptos-listado.component';
import { EventoDetalleComponent } from './eventos/evento-detalle/evento-detalle.component';
import { EventoListadoCompletoComponent } from './eventos/evento-listado-completo/evento-listado-completo.component';
import { InscriptosFormComponent } from './inscriptos/inscriptos-form/inscriptos-form.component';
import { EventoDetalleAsistenteComponent } from './eventos/evento-detalle-asistente/evento-detalle-asistente.component';
import { UsuarioFormComponent } from './usuario-form/usuario-form.component';
import { EventoModificarComponent } from './eventos/evento-modificar/evento-modificar.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'usuario_nuevo', component: UsuarioFormComponent },
  { path: 'evento_nuevo', component: EventosFormComponent,
    data:{
    order:'some value'
  }
  },
  { path: 'inscripcion', component: InscriptosFormComponent, 
  data:{
    order:'some value'
  }
  },
  { path: 'evento_listado', component: EventosListadoComponent,
    data:{
      order:'some value'
    }
  },
  { path: 'evento_listadoCompleto', component: EventoListadoCompletoComponent},
  { path: 'login', component: LoginComponent },
  { path: 'listaInscriptos', component: InscriptosListadoComponent,
  data:{
    order:'some value'
  }
  },
  { path: 'detalleEvento', component: EventoDetalleComponent,
    data:{
    order:'some value'
  }
  },
  { path: 'detalleEventoAsistente', component: EventoDetalleAsistenteComponent,
  data:{
  order:'some value'
}
},
{ path: 'evento_modificar', component: EventoModificarComponent,
    data:{
      order:'some value'
    }
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
