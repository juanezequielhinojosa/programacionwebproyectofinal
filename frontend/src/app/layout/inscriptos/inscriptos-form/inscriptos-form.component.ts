import { Component, OnInit } from '@angular/core';
import { InscriptosFormService } from 'src/app/services/inscriptos-form.service';
import { Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-inscriptos-form',
  templateUrl: './inscriptos-form.component.html',
  styleUrls: ['./inscriptos-form.component.css']
})
export class InscriptosFormComponent implements OnInit {

  constructor(private route:ActivatedRoute, private router:Router,private inscriptosFormService:InscriptosFormService) { }

      id_inscripciones='';
      nombre ='';
      apellido ='';
      dni ='';
      celular ='';
      codigo_pago ='';
      codigo_inscripcion ='';
      eventos_id ='';
      

  ngOnInit(): void {
    this.route.queryParams.subscribe(params=>{
      this.eventos_id=params['eventoId'];
      
    });

  }
  submitInscripcion(){
    this.inscriptosFormService.postInscripcion(this.id_inscripciones,this.nombre,this.apellido,this.dni,this.celular,this.codigo_pago,this.codigo_inscripcion,this.eventos_id).subscribe(respuesta => {
       console.log(respuesta)
       if(respuesta['ok']==true){
          alert('se agrego correctamente')
          this.router.navigate(['/evento_listadoCompleto']);
      }else{
       alert('error intente mas tarde');
      }
      
       
    });
   }

}
