import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptosFormComponent } from './inscriptos-form.component';

describe('InscriptosFormComponent', () => {
  let component: InscriptosFormComponent;
  let fixture: ComponentFixture<InscriptosFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriptosFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptosFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
