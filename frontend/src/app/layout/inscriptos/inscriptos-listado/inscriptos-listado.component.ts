import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { InscriptosListadoService } from 'src/app/services/inscriptos-listado.service';

@Component({
  selector: 'app-inscriptos-listado',
  templateUrl: './inscriptos-listado.component.html',
  styleUrls: ['./inscriptos-listado.component.css']
})
export class InscriptosListadoComponent implements OnInit {

  constructor(private route:ActivatedRoute,private router:Router,private inscriptosListadoService:InscriptosListadoService) { }

  inscriptos: any []=[];
  idcurso='';
  titulo='';
  
  
  ngOnInit(): void {
    
    this.route.queryParams.subscribe(params=>{
      
      this.idcurso=params['idCurso']
      this.titulo=params['titulo']
      this.inscriptosListadoService.postInscriptos(this.idcurso).subscribe(respuesta => {
        console.log(respuesta)
        this.inscriptos=respuesta
        if(respuesta.length > 0){
         alert('Hay inscriptos en este curso')
        }else{
          alert('No hay inscriptos en este curso');
        }
        
      })
     
      
    });
    
  }

}
