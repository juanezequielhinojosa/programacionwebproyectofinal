import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptosListadoComponent } from './inscriptos-listado.component';

describe('InscriptosListadoComponent', () => {
  let component: InscriptosListadoComponent;
  let fixture: ComponentFixture<InscriptosListadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriptosListadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptosListadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
