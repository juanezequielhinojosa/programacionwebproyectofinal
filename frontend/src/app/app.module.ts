import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './layout/main/main.component';
import { HeaderComponent } from './layout/header/header.component';
import { LayoutModule } from './layout/layout.module';
import { EventosFormComponent } from './layout/eventos/eventos-form/eventos-form.component';
import { EventosListadoComponent } from './layout/eventos/eventos-listado/eventos-listado.component';
import { HttpClientModule } from '@angular/common/http';//1-paso
import { FormsModule } from '@angular/forms';
//sevicios

import { EventoFormService } from './services/evento-form.service';
import { UsuariosService } from './services/usuarios.service';
import { EventoListadoService } from './services/evento-listado.service';
import { EventoBuscadoService } from './services/evento-buscado.service';
import { InscriptosListadoService } from './services/inscriptos-listado.service';
import { InscriptosFormService } from './services/inscriptos-form.service';
import { UsuarioFormService } from './services/usuario-form.service';
import { EventoBorradoService } from './services/evento-borrado.service';
import { EventoModificarService } from './services/evento-modificar.service';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LayoutModule,
    FormsModule,
    HttpClientModule//1-paso
  ],
  providers: [
    EventoFormService,
    UsuariosService,
    EventoListadoService,
    EventoBuscadoService,
    InscriptosListadoService,
    InscriptosFormService,
    UsuarioFormService,
    EventoBorradoService,
    EventoModificarService
  
  ],
  bootstrap: [AppComponent]
  
})
export class AppModule { }
