const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser'); //bodyParser nos permite reicibir parametros por POST
const mysqlConnection = require('../../database.js');
var jsonParser = bodyParser.json();
var urlencodedParser = bodyParser.urlencoded({ extended: false });




// GET all eventos
router.get('/', (req, res) => {
    mysqlConnection.query('SELECT * FROM eventos', (err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

// GET all eventos por Id de Usuario Logueado
router.get('/listarPorIdUsuarioLogueado/:id',async function(req, res){
   //console.log(req);
   const {id} = req.params;
   // console.log(id);
   
    let sql ='SELECT * FROM eventos WHERE id_usuarios = ?';
    var valores = [id];
    mysqlConnection.query(sql,valores,async(err, rows, fields) => {
        if (!err) {
            res.json(rows);
        } else {
            console.log(err);
        }
    });
});

//listar eventos por id
//router.get('/evento', async function(req, res) {   
//    const { id } = req.body;
//    console.log(id);
//    let sql ='SELECT * FROM eventos WHERE id_eventos = ?';
//    var valores = [id];
//    mysqlConnection.query(sql,valores, async(err, rows, fields) => {
//        if (!err) {           
//            res.json({
//                ok: true,
//                eventos: rows[0]                
//            });
//        } else { 
//            res.status(400).json({
//                ok: false,
//                mensaje: 'Hubo un problema para generar la respuesta'
//            });
//       }
 //   });

//});

////////////


///////////



// GET eventos por email de usuario
router.get('/mail', (req, res) => {
    const { email } = req.body;
    mysqlConnection.query('CALL PROC_CONSULTAR_EVENTOS_POR_EMAIL(?)', [email], (err, rows, fields) => {
        if (!err) {
            res.json(rows[0]);
        } else {
            console.log(err);
        }
    });
});



// DELETE An Evento
router.delete('/evento/eliminar/:id', (req, res) => {
    const { id } = req.params;
    console.log(id);
    mysqlConnection.query('DELETE FROM eventos WHERE id_eventos = ?', [id], (err, rows, fields) => {        
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});


//INSERT An Evento
router.post('/evento/', urlencodedParser, (req, res) => {//se accede con el barra evento
    const { titulo, descripcion,imagen,fecha, precio,duracion,capacidad,modalidad,ponente1,ponente2,ponente3,link1,link2,link3, id_usuarios } = req.body;

    let sql = 'INSERT INTO eventos(titulo, descripcion,imagen,fecha, precio,duracion,capacidad,modalidad,ponente1,ponente2,ponente3,link1,link2,link3, id_usuarios) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
    var valores = [titulo, descripcion,imagen,fecha,precio,duracion,capacidad,modalidad,ponente1,ponente2,ponente3,link1,link2,link3, id_usuarios];//paso estos valores

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
     }
    });
});
//modificar Evento
router.put('/evento/modificarCurso/:id',async function(req, res) {//se accede con el barra evento
    const {id} = req.params;
    
    //let sql = 'UPDATE eventos SET titulo=titulo, descripcion=descripcion, imagen=imagen, fecha=fecha, precio=precio, duracion=duracion, capacidad=capacidad, modalidad=modalidad, ponente1=ponente1, ponente2=ponente2, ponente3=ponente3, link1=link1, link2=link2, link3=link3, id_usuarios=id_usuarios WHERE id_eventos=34';
    //

    mysqlConnection.query('UPDATE eventos SET ? WHERE id_eventos=?', [req.body,id]);
    res.json({ ok: true });
       
   });


///////////

// INSERT An Usuario
router.post('/evento/registrar', urlencodedParser, (req, res) => {//se accede con el barra evento
    const { email, password} = req.body;

    let sql = 'INSERT INTO usuarios(email, password) VALUES (?,?)';
    var valores = [email, password];//paso estos valores

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});
////////////


// INSERT An Inscripcion
router.post('/evento/inscripcion', urlencodedParser, (req, res) => {
    const { id_inscripciones, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion,eventos_id } = req.body;


    let sql = 'INSERT INTO inscripciones(id_inscripciones, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion,eventos_id) VALUES (?,?,?,?,?,?,?,?)';
    var valores = [id_inscripciones, nombre, apellido, dni, celular, codigo_pago, codigo_inscripcion,eventos_id];

    mysqlConnection.query(sql, valores, (err, rows, fields) => {
        if (!err) {
            res.json({ ok: true });
        } else {
            console.log(err);
        }
    });
});
///listar inscriptos por ventos
router.post('/inscriptos', async function(req, res) {   
    const { id } = req.body;
    console.log(id);
    let sql ='SELECT * FROM inscripciones WHERE eventos_id = ?';
    var valores = [id];
    mysqlConnection.query(sql, valores, async(err, rows, fields) => {
        if (!err) {            
            res.json(rows);
        } else {
            res.status(400).json({
                ok: false,
                mensaje: 'Hubo un problema para generar la respuesta'
            });
       }
      // res.json({
       // ok: true,
       // pedidos: rows                
    //});

     


    });
});

///////


////////

module.exports = router;