const mysql = require('mysql');//utilizaremos librerias mysql

const mysqlConnection = mysql.createConnection({//solicitud de la creaccion de una conexion al motor mysql atravez de la libreria mysql
  //debo pasarle datos para esa conexion : el host , el local host , ip si es externa la base de datos, el ussuario de la bd root, el pasword
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_tickets',//el nombre de la base de datos
    multipleStatements: true//configuracion  que indica si va a ser posible manejar multiples conexiones en este caso si
});

mysqlConnection.connect(function(err) {//aqui damos la orden la concexion es una funcion que en caso de error mustra una  un error y sino dice la bd esta conectada
    if (err) {
        console.error(err);
        return;
    } else {
        console.log('db esta conectada');
    }
});

module.exports = mysqlConnection;//este modulo se exporta y ya esta disponible para quien lo solicite
//esto se escribe solo una vez

